# classmeet

Classmeet is a frontend application for the virtual classroom solutions offered by chiguru.tech. This is written in django and uses postgresql as its database

Dependencies :

1. Sentry : For Error and exception logging
2. Django and Python 3.6+
3. An SMTP Server with credentials
4. Nginx or any other web server for serving static files
5.

Config :

Create config file in classmeetweb/cmconfig/config.ini referring to config.ini.sample in the same path

```bash

python manage.py migrate
python manage.py loaddata initial_data/subscriptions.json
python manage.py loaddata initial_data/user.json
python manage.py changepassword hello@chiguru.tech
python manage.py runserver

```

django_compressor has provide a Django command for us to convert scss files to css files.

```bash

python manage.py collectstatic
python manage.py compress --force
```
