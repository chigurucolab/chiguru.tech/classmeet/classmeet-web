
var name = localStorage.getItem("classmeetname")
var userId = localStorage.getItem("classmeetuserId")
if (name != "null"){
  $("#name").val(name);
}
function uuidv4() {
  return 'p-yxxx-xxxy-xxyx-4yxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
if (userId == null){
  userId = uuidv4() 
  localStorage.setItem("classmeetuserId",userId)
}
$("#userId").val(userId);
$("#session-join").click(function () {
  localStorage.setItem("classmeetname", $("#name").val())
})