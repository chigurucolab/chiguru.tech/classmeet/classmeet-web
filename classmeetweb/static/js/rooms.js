 function copyToClipboard(element) {
     $(element).select();
     document.execCommand("copy");
     $('[data-toggle="tooltip"]').tooltip('toggle');
 }

 $(".copy_room_user_id").mouseenter(function(){
     $(this).children().first().show()
 }).mouseleave(function () {
     $(this).children().first().hide();
     $(this).children().first().text("Copy");  
    })
 
 
 function copyLink(room_link){
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(room_link).select();
    document.execCommand("copy");
    $temp.remove();
    
 }

 $(".copy_room_user_id").click(function(){
    $(this).children().first().text("Copied!")
 })
 function startSession(roomname,room_id, room_link){
     $("#display_room_name").text(roomname)
     $("#display_room_link").val(room_link)
     $("#display_room_id").val(room_id)
     $('#room_start_modal').modal('show')
 }

//  function openModal(element) {
//      $(element).modal();
//  }

//  function updateLink(url, room_user_id = null) {
//      document.getElementById('inviteurl').value = url;
//      document.getElementById('room_id').value = room_user_id
//  }

//  function updateRoomName(url, no_of_session, last_session) {
//      document.getElementById('currentRoomName').innerHTML = url;
//      $("#no_of_sessions").text(no_of_session)
//      if (last_session != "None") {
//          $("#last_session").text(last_session)
//      } else {
//          $("#last_session").text("Never")
//      }
//  }

//  $(document).ready(function () {
//      document.getElementById("roomListItems").firstElementChild.click();
//  });

//  function showCheckbox(check,inp){
//      var checkBox = document.getElementById(check);

//      var input = document.getElementById(inp);

//      if(checkBox.checked == true){
//          input.style.display = "block";
//      }else{
//          input.style.display = "none";
//      }
//  }


