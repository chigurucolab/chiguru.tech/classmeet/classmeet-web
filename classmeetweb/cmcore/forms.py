from hashlib import sha256
import unicodedata
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from allauth.account.forms import SignupForm
from .models import User

class UserCreationForm(SignupForm):
    fullname = forms.CharField(label='Name',widget=forms.TextInput(attrs={'placeholder': 'Name'}))
    

    def save(self, request):

        user = super(UserCreationForm, self).save(request)
        user.fullname = request.POST.get("fullname","Anonymous")
        user.userid = sha256(unicodedata.normalize('NFKD', user.email).encode('ascii', 'ignore')).hexdigest()
        user.save()
        
        return user

class UserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('email',) 
