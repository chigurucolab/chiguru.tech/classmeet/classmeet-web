import json
import datetime
from pathlib import Path
import xmltodict
import requests 
import jwt
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.conf import settings 
from django.http import request, JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from cmweb.models import Rooms, RoomRecording, RoomHistory
from cmcore.utils import prepare_url, prepare_for_create, prepare_for_moderator_join



@login_required
@require_http_methods(['POST'])
def session_start(request):
    # TODO: Currently set to UserID. Need to chagne this to Firstname. A fix in the registration page is needed to get this info
    room_id = request.POST.get('room_id',None)
    if not room_id:
        return redirect(reverse('home'))
    room = get_object_or_404(Rooms, room_user_id=room_id)
    create = False
    if not room.is_session_running:
        create = True
    
    
    moderator_name = request.user.fullname  if request.user.fullname else "Anonymous"
    meeting_id = room_id
    password = ""
    record = "true" if room.creator.subscription.recording_allowed else "false"
    maxParticipants = room.creator.subscription.participants_allowed
    duration = room.creator.subscription.min_allowed
    user_id = room.creator.userid
    
    if create:
        parallel_sessions = room.creator.subscription.parallel_sessions
        current_running_sessions = Rooms.objects.filter(creator=request.user, is_session_running=True).count()

        if room.room_type == "user" and (current_running_sessions < parallel_sessions ):
            # Frist, create the room. Rooms are not persistant and needs to be created everytime a session is about to be started
            scheme = 'https' if not settings.DEBUG else request.scheme
            meeting_url = scheme+"://"+request.get_host()

            # Setting the guest policy right
            if room.room_guest_policy == "open" or room.room_guest_policy == "password":
                guestPolicy = "ALWAYS_ACCEPT"
            elif room.room_guest_policy == "approval":
                guestPolicy = "ASK_MODERATOR"
            else:
                guestPolicy = "ALWAYS_ACCEPT"
            
            # Set the webcam policy and participant mute policy
            lockSettingsDisableCam = "true" if room.room_participant_webcam else "false"
            muteOnStart = "true" if room.room_mute_all_on_start else "false"
            create_query_params = prepare_for_create(
                moderator_name,
                room.room_name,
                meeting_id,
                meeting_url,
                record = record, 
                guestPolicy = guestPolicy,
                muteOnStart = muteOnStart,
                lockSettingsDisableCam = lockSettingsDisableCam,
                maxParticipants=maxParticipants,
                duration=duration,
                )
            create_url = prepare_url("create",create_query_params)
            # creating HTTP response object from given url 
            create_room_resp = requests.get(create_url)
            resp_json = json.loads(json.dumps(xmltodict.parse(create_room_resp.content)))
            if resp_json["response"]["returncode"] != "SUCCESS":
                return redirect(reverse("home"))
        else:
            return redirect(reverse('home'))

    
    query_params = prepare_for_moderator_join(moderator_name,meeting_id,user_id)
    moderator_redirect_url = prepare_url("join",query_params)
    mod_join_resp = requests.get(moderator_redirect_url)
    mod_join_resp_json = json.loads(json.dumps(xmltodict.parse(mod_join_resp.content)))
    if mod_join_resp_json["response"]["returncode"] == "SUCCESS":
        session_url = mod_join_resp_json["response"]['url']
        # This means, the session is about to start. Update the room status.
        if not room.is_session_running:
            now = datetime.datetime.now()
            room.is_session_running = True
            room.room_session_count = room.room_session_count + 1 
            room.last_session = now
            room.save()
            room_history = RoomHistory.objects.create(session=room,session_name=room.room_name+"-"+now.strftime("%Y:%m:%d-%H:%M"),session_start_datetime = now,session_started_by=request.user)
            room_history.save()
        return redirect(session_url)


@require_http_methods(["POST"])
@csrf_exempt
def record_callback(request):
    data = request.body.decode('utf-8')
    decoded_response = jwt.decode(data.split("=")[1],settings.CLASSMEET_SALT,algorithms=['HS256']) 
    room_user_id = decoded_response["meeting_id"]
    rid = decoded_response["record_id"]
    room = get_object_or_404(Rooms, room_user_id=room_user_id)
    if room:
        # Validate if room recording is available
        is_rr = RoomRecording.objects.filter(recording_id="rid")
        if len(is_rr) == 0 :
            now = datetime.datetime.now()
            rr = RoomRecording.objects.create(room=room, name="%s recording on %s  " %(room.room_name,now.strftime("%d-%m-%Y:%H:%M")),recording_id=rid,is_active=True)
            if not settings.DEBUG:
                root_directory = Path("/var/bigbluebutton/%s"%(rid))
                rr.recording_size = sum(f.stat().st_size for f in root_directory.glob('**/*') if f.is_file())
            else:
                from random import randint
                rr.recording_size = randint(0,5000)
            rr.save()
            room.room_total_storage_size = room.room_total_storage_size + rr.recording_size
            room.save()
            response = JsonResponse({'status':'true','message':"End Room logged"}, status=200)
        else: 
            response = JsonResponse({'status':'true','message':'Room recording already exists'}, status=200)
    else:
        # TODO: Handle notification of room not found.
        #raise Exception("Room with %s not found "%(room))
        response = JsonResponse({'status':'false','message':'Room not found'}, status=200)
    return response
    

# "GET /session/end/?recordingmarks=false&meetingID=ram-tes-470f HTTP/1.1"
@require_http_methods(["GET"])
def end_room(request):
    room_user_id = request.GET.get("meetingID")
    room_recorded = True if request.GET.get("recordingmarks",None) else False
    room = get_object_or_404(Rooms, room_user_id=room_user_id)
    room.is_session_running=False 
    room.save()
    # TODO: This is a very hacky logic and requires revisit. At this point in time, we are heavily relying on the create logic to ensyre there is always one sessoin.
    room_history = RoomHistory.objects.filter(session=room,session_end_datetime__isnull=True).latest('session_start_datetime')
    room_history.session_end_datetime = datetime.datetime.now()
    room_history.session_was_recorded = room_recorded
    room_history.save()
    response = JsonResponse({'status':'true','message':"End Room logged"}, status=200)
    return response

@login_required
@require_http_methods(["GET"])
def user_profile(request):
    return render(request,'pages/account.html')