import xml
from hashlib import sha256
import json, xmltodict
import xml.etree.ElementTree as ET 
from urllib.parse import urlencode

from django.conf import settings
  

def prepare_url(method,query_params,salt=settings.CLASSMEET_SALT):

    prepared = "%s%s%s" % (method, query_params , salt)
    checksum = sha256(prepared.encode('utf-8')).hexdigest()
    url = settings.CLASSMEET_HOST +  method + "/?" + query_params + "&checksum=" + checksum
    return url

def prepare_for_create(moderator_name, meeting_name, meeting_id,meeting_url,password="",lockSettingsDisableCam="false",muteOnStart="false",guestPolicy="ALWAYS_ACCEPT",record="false",autoStartRecording="false",
    allowStartStopRecording="false",maxParticipants=40,duration=5,moderatorPW=settings.CLASSMEET_DEFAULT_MODERATORPW,
    attendeePW=settings.CLASSMEET_DEFAULT_ATTENDEEPW,redirect="false"):
    # if settings.DEBUG:
    END_CALLBACK_URL = meeting_url+settings.END_CALLBACK_URL 
    RECORD_CALLBACK_URL = meeting_url+settings.RECORD_CALLBACK_URL
    # else: 
    #     END_CALLBACK_URL = settings.END_CALLBACK_URL 
    #     RECORD_CALLBACK_URL = settings.RECORD_CALLBACK_URL

    return urlencode((
        ('name',meeting_name),
        ('fullName', moderator_name),
        ('meetingID', meeting_id),
        ('password', password),
        ('record',record),
        ('autoStartRecording',autoStartRecording),
        ('maxParticipants',maxParticipants),
        ('duration',duration),
        ('redirect',redirect),
        ('moderatorPW',moderatorPW),
        ('attendeePW',attendeePW),
        ('guestPolicy',guestPolicy),
        ('lockSettingsDisableCam',lockSettingsDisableCam),
        ('muteOnStart',muteOnStart),
        ('logo',"https://classmeet.chiguru.tech/images/logo_compressed.png"),
        ('moderatorOnlyMessage',"Link for participants to join : %s/app/%s"%(meeting_url,meeting_id)),
        ('meta_endCallbackUrl',END_CALLBACK_URL),
        ('meta_bbb-recording-ready-url',RECORD_CALLBACK_URL),
        ))


def prepare_for_moderator_join(moderator_name,meeting_id,userID,moderatorPW=settings.CLASSMEET_DEFAULT_MODERATORPW,
    redirect="false"):
    return urlencode((
        ('meetingID',meeting_id),
        ('password',moderatorPW),
        ('fullName', moderator_name),
        ('userID',userID),
        ('userdata-bbb_client_title','ClassMeet'),
        ('userdata-bbb_display_branding_area','true'),
        ('userdata-bbb_ask_for_feedback_on_logout',"true"),
        ('userdata-bbb_mirror_own_webcam','true'),
        ('redirect',redirect)
        ))
    
def prepare_for_attendee_join(moderator_name,meeting_id,userID,muted="false",attendeePW=settings.CLASSMEET_DEFAULT_ATTENDEEPW,redirect="false"):
    return urlencode((
        ('fullName', moderator_name),
        ('meetingID',meeting_id),
        ('password',attendeePW),
        ('userID',userID),
        ('userdata-bbb_client_title','ClassMeet'),
        ('userdata-bbb_display_branding_area','true'),
        ('userdata-bbb_mirror_own_webcam','true'),
        ('userdata-bbb_ask_for_feedback_on_logout',"true"),

        ('userdata-bbb_skip_check_audio','true'),
        ('userdata-bbb_force_listen_only','false'),
        ('userdata-bbb_listen_only_mode','false'),
        ('redirect',redirect)
    ))

def prepare_for_streamer_join(meeting_id,attendeePW=settings.CLASSMEET_DEFAULT_ATTENDEEPW,redirect="true"):
    return urlencode((
        ('fullName', "ChiguruStreamer"),
        ('meetingID',meeting_id),
        ('password',attendeePW),
        ('userID',"p_chiguru_streamer"),
        ('userdata-bbb_client_title','ClassMeet'),
        ('userdata-bbb_display_branding_area','true'),
        ('redirect',redirect)
    ))

def prepare_for_meeting_running(meeting_id):
    return urlencode((
        ('meetingID',meeting_id),
        ))   

def prepare_for_info(meeting_id,moderatorPW=settings.CLASSMEET_DEFAULT_MODERATORPW,redirect="false"):
    return urlencode((
        ('meetingID',meeting_id),
        ('password',moderatorPW)
        ))


def prepare_to_end(meeting_id):

    return urlencode((('meetingID',meeting_id),('password',moderatorPW)))


