from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.http import urlencode
from django.urls import reverse
from django.utils.html import format_html

from .forms import UserCreationForm, UserChangeForm
from .models import User, Subscription, UserOrgMapping, Organisation


class UserAdmin(UserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = User
    list_display = ('fullname', 'email','is_staff', 'is_active',)
    list_filter = ('subscription','is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('fullname','userid','email', 'password')}),
        ('Subscription', {'fields': ('subscription',)}),
        ('Permissions', {'fields': ('is_staff', 'is_active','is_superuser')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
         ),
    )
    search_fields = ('email','fullname')
    ordering = ('email','subscription')
    # date_hierarchy = 'created'

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('subscription_name', 'recording_allowed', 'min_allowed', 'participants_allowed','is_active','subscription_cost','subscription_period','no_users')
    list_filter = ('subscription_period','recording_allowed','is_active')
    search_fields = ('subscription_name',)
    ordering = ('participants_allowed','subscription_cost')

    def no_users(self,obj):
        sub_count = User.objects.filter(subscription=obj).count()
        url = (
            reverse("admin:cmcore_user_changelist")
            + "?"
            + urlencode({"subscription__id": obj.id})
        )
        return format_html('<a href="{}">{} Users</a>', url, sub_count)

    no_users.short_description = "No of users"

admin.site.register(User, UserAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Organisation)
admin.site.register(UserOrgMapping)
