from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .managers import UserManager
from django.dispatch import receiver
from django.db.models.signals import post_save


class Subscription(models.Model):
    subscription_payment_choices = ((
        ('Month','monthly'),
        ('Year','yearly')
    ))
    subscription_name = models.CharField(max_length=200)
    recording_allowed = models.BooleanField(default=False)
    min_allowed = models.PositiveIntegerField(
        help_text="How many minutes can the sessions in this subscription go on", default=60)
    participants_allowed = models.PositiveIntegerField(
        default=40, help_text="How many participants allowed")
    parallel_sessions = models.PositiveIntegerField(
        default=1, help_text="Parallel sessions allowed")
    is_active = models.BooleanField(default=True)
    subscription_cost = models.PositiveIntegerField(default=0,blank=True)
    subscription_period = models.CharField(choices=subscription_payment_choices, max_length=30,default="monthly")
    storage_allowed = models.PositiveIntegerField(default=0,help_text="Storage limit allowed for storing recordings")
    # live_stream_allowed = models.BooleanField(default=False)
    # live_stream_cost = models.PositiveIntegerField(default=0)
    # live_stream_viewer_cost = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.subscription_name


class User(AbstractUser):
    username = models.CharField(max_length=100,default="",verbose_name="User Name",help_text="Enter ")
    email = models.EmailField(_('email address'), unique=True)
    fullname = models.CharField(max_length=100,default="",verbose_name="Your Name")
    subscription = models.ForeignKey(Subscription, on_delete=models.SET_NULL,blank=True, null=True)
    userid = models.CharField(max_length=50,blank=True, null=True, verbose_name="User ID")

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullname',]

    objects = UserManager()

    def __str__(self):
        return self.email


class Organisation(models.Model):
    org_type_choices = (
        ('user', 'User'),
        ('org', 'Organisation'),
        ('grassroot', 'Grassroot')
    )
    org_name = models.CharField(max_length=200)
    org_type = models.CharField(max_length=100, default="user")
    org_owner = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    org_admin = models.ManyToManyField(
        User, related_name="org_owner")
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    org_subscription = models.ForeignKey(
        Subscription, on_delete=models.CASCADE)

    def __str__(self):
        return self.org_name


class UserOrgMapping(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)


@receiver(post_save, sender=User)
def print_only_after_deal_created(sender, instance, created, **kwargs):
    if created:
        # Create a personal Organisation
        subscription = Subscription.objects.get(id=1)
        instance.subscription = subscription
        instance.save()
