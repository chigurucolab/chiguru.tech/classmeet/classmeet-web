from django.urls import path
from .views import session_start, end_room, user_profile, record_callback
urlpatterns = [
    path('start/', session_start, name='session_start'),
    path('user_profile/', user_profile, name='user_profile'),
    path('end/',end_room, name="end_room"),
    path('record/',record_callback, name="session_record_callback")


]
