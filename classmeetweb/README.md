ClassMeet is a highly opinionated and super customized frontend implementation for BBB. At this stage, this project is not aimed at being generic to serve everyone's purpose. The project is primarily intended for users of ClassMeet for them to ensure that they have access to the code of the service they are using. Repeating again, we may or may not make this generic. 



Dependencies :

Django 2.2
Python3.6
Postgresql
Redis
Celery

Steps to install

1. Create the venv and install from requirements.txt 
2. python manage.py migrate
3. python manage.py loaddata initial_data/subscriptions.json
4. python manage.py loaddata initial_data/user.json
5. python manage.py changepassword hello@chiguru.tech
6. python manage.py runserver


Note: There are some components that we have not opened up yet. For example the streamer or the image id's. These are basically VM snapshots whose internal code, we have not even brought to git. Once those are brought in, they will also be made public. Currently we are not focussing on the same.