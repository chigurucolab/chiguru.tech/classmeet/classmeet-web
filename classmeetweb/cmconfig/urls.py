from django.conf import settings
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('session/', include('cmcore.urls')),
    path('/session/', include('cmcore.urls')),
    path('account/', include('cmcore.urls')),
    path('', include('cmweb.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
] + urlpatterns
    admin.site.site_title = "Dev ClassMeet"
    admin.site.site_header = "Dev ClassMeet"


else:
    admin.site.site_header = "ClassMeet(PRODUCTION)"
    admin.site.site_title = "ClassMeet"