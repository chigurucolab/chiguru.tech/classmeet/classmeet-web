from django.conf import settings

def from_settings(request):
    if settings.DEBUG:
        return {
            'ENVIRONMENT_NAME': "Non Production",
            'ENVIRONMENT_COLOR': "green",
        }
    else:
        return {
            'ENVIRONMENT_NAME': "Production",
            'ENVIRONMENT_COLOR': "red",
        }