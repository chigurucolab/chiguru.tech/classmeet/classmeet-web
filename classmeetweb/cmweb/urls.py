from django.urls import path

# Generic views
from .views import home, TermsPageView, AboutPageView
# Room specific views
from .views import update_room, join_room, myrooms, create_room, delete_room
#Recording specific views
from .views import update_recording, view_recording
#Stream specific views
from .views import get_streamer_link, go_live, destroy_streamer, watch, start_scheduled_stream, stream_comments_moderator
# Comments specific views
from .views import stream_ask_question, get_stream_questions


urlpatterns = [
    path('', myrooms, name="home"),
    path('about/', AboutPageView.as_view(), name='about'),
    path('terms/', TermsPageView.as_view(), name='terms'),

    path('create_room/', create_room, name='create_room'),
    #path('app/', myrooms, name="myrooms"),
    #path('app/<str:room_user_id>/info/',get_room_info , name="get_room_info"),
    path('update_room/<str:room_id>/',
         update_room, name="update_room"),
     path('update_recording/<str:recording_id>/', 
         update_recording, name="update_recording"),
    path('delete_room/<str:room_name>/<str:room_id>/',
         delete_room, name='delete_room'),
    # Recording specific urls
    path('recording/<str:recording_id>/',view_recording,name="view_recording"),

    # Stream specific urls
    path('go_live/',go_live,name="go_live"),
    path('live/',go_live,name="live"),
    path('get_streamer_link/',get_streamer_link,name="streamer_link_generator"),    
    path('destroy_streamer/',destroy_streamer,name="destroy_streamer"),
    path('watch/<str:stream_id>/', watch, name="watch"),
    path('start_scheduled_stream/',start_scheduled_stream,name="start_scheduled_stream"),
    path('stream_comments_moderator/<str:stream_id>/',stream_comments_moderator,name="stream_comments_moderator"),

    # Comments specific URL
    path('stream/ask_question/',stream_ask_question,name='stream_ask_question'),
    path('stream/get_stream_questions/<str:stream_id>/',get_stream_questions,name="get_stream_questions"),
    path('stream/get_stream_questions/<str:stream_id>/<int:sqid>/',get_stream_questions,name="get_stream_questions"),
    # This should always remain at the end. Write all paths above this line.
    path('<str:room_user_id>/', join_room, name="join_room"),

]
