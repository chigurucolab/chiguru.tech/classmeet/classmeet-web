from celery.decorators import task
from celery.utils.log import get_task_logger
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.conf import settings
from .models import Stream, Rooms
import requests
import libcloud

logger = get_task_logger(__name__)

root_url = None
if settings.DEBUG:
    root_url = "http://localhost:8000"
else:
    root_url = "https://classmeet.chiguru.tech"


@task(name="create_streamer_task")
def create_streamer_task(stream_id, room_id):
    """Creates the streamer server"""
    # logger.info("Sent feedback email")
    # logger.info(stream_id,room_id)
    cls = libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.DIGITAL_OCEAN)
    driver = cls(settings.CLOUD_KEY) 
    KEYPAIR_NAME = settings.CLOUD_KEYPAIR
    size = [s for s in driver.list_sizes() if s.id == "c-2"]
    image = [i for i in driver.list_images() if i.id == settings.CLOUD_SNAPSHOT_ID]
    location = [l for l in driver.list_locations() if l.id == settings.CLOUD_REGION]
    node = driver.create_node(stream_id, image=image[0], size=size[0], location=location[0],ex_ssh_key_ids=KEYPAIR_NAME)
    logger.info(node)
    status = False
    while status == False:
        nodes = driver.list_nodes()
        cur_node = cur_node = [n for n in nodes if n.uuid == node.uuid][0]
        if cur_node.state == "running":
            status = True
        else:
            status = False 
    stream = get_object_or_404(Stream,stream_id=stream_id)
    if stream:
        stream.stream_status = "ready"
        stream.stream_link = cur_node.public_ips[0]
        stream.stream_server_uuid = cur_node.uuid
        stream.save()



@task(name="destroy_streamer_task")
def destroy_streamer_task(stream_id):
    cls = libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.DIGITAL_OCEAN)
    driver = cls(settings.CLOUD_KEY) 
    nodes = driver.list_nodes()
    logger.info(nodes)
    cur_node = [n for n in nodes if n.name == stream_id][0]
    logger.info(cur_node)
    if cur_node:
        driver.destroy_node(cur_node)
        stream = get_object_or_404(Stream,stream_id=stream_id)
        if stream:
            stream.stream_status = "destroyed"
            stream.save()
    else:
        logger.info("Streaming node : %s not found."%stream_id)