from django.contrib import admin
from .models import Rooms, RoomRecording, RoomHistory, Stream, StreamQuestions
from django.utils.http import urlencode
from django.urls import reverse
from django.utils.html import format_html


class RoomAdmin(admin.ModelAdmin):
    list_display = ('room_name', 'room_user_id', 'room_id','creator','is_active','recording_count','room_history')
    list_filter = ('is_active','is_session_running','room_guest_policy')
    search_fields = ('room_user_id','room_name')
    
    def recording_count(self, obj):
        count = RoomRecording.objects.filter(room=obj).count()
        url = (
            reverse("admin:cmweb_roomrecording_changelist")
            + "?"
            + urlencode({"room__id": obj.id})
        )
        return format_html('<a href="{}">{} Recordings</a>', url, count)
    
    def room_history(self,obj):
        count = RoomHistory.objects.filter(session=obj).count()
        url = (
            reverse("admin:cmweb_roomhistory_changelist")
            + "?"
            + urlencode({"session__id": obj.id})
        )
        return format_html('<a href="{}">{} Sessions</a>', url, count)
        
    recording_count.short_description = "No of Recordings"
    room_history.short_description = "History count"

class RoomRecordingAdmin(admin.ModelAdmin):
    list_display = ('room','recording_id','recording_size__mb','recording_visibility')
    list_filter = ('is_active','recording_visibility')
    search_fields =('name','room__room_name')

    def recording_size__mb(self,obj):
        size = obj.recording_size / 1024
        return size
    
    recording_size__mb.short_description = "Size (MB)"

class RoomHistoryAdmin(admin.ModelAdmin):
    list_display = ('session_internal_id','session_start_datetime','session_end_datetime','session_participants','session_was_recorded','session_started_by')
    list_filter = ('session_was_recorded',)
    search_fields = ('session_internal_id',)

class StreamAdmin(admin.ModelAdmin):
    list_display = ('stream_room','stream_id','stream_link','stream_start_time','stream_status','stream_end_time')
    search_fields = ('stream_room','stream_id','stream_link')
    list_filter = ('stream_status',)

class StreamQuestionsAdmin(admin.ModelAdmin):
    list_display = ('name','comment','stream')
    search_fields = ('name','comment','stream')

class RoomHistoryAdmin(admin.ModelAdmin):
    list_display = ('session_internal_id','session_start_datetime','session_end_datetime','session_participants','session_was_recorded','session_started_by')
    list_filter = ('session_was_recorded',)
    search_fields = ('session_internal_id',)

admin.site.register(Rooms, RoomAdmin)
admin.site.register(RoomRecording, RoomRecordingAdmin)
admin.site.register(RoomHistory, RoomHistoryAdmin)
admin.site.register(Stream,StreamAdmin)
admin.site.register(StreamQuestions,StreamQuestionsAdmin)
