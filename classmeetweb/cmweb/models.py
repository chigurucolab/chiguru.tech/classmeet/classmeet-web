from hashlib import sha256
import time, uuid
from django.db import models
from cmcore.models import User, Organisation
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.postgres.fields import JSONField

class Rooms(models.Model):

    room_guest_policy_choices = (
        ("open","Anyone with the link can join"),
        ("approval","Moderator should approve users"),
        ("password","Allow on entering a password")
        )

    is_active = models.BooleanField(default=False)
    is_session_running = models.BooleanField(default=False)
    room_name = models.CharField(max_length=200, blank=False, null=False)
    room_user_id = models.CharField(max_length=200, blank=False, null=False,default="")
    room_id = models.CharField(
        max_length=100, blank=True, null=True, default="")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    creator = models.ForeignKey(User, on_delete=models.SET_NULL,related_name="room_creator",null=True)
    owner = models.ManyToManyField(User,related_name="room_owners",blank=True,null=True)
    room_type = models.CharField(max_length=20,default="user")
    org_belongs_to = models.ForeignKey(Organisation,blank=True,null=True,on_delete=models.SET_NULL)
    room_mute_all_on_start = models.BooleanField(default=False) # Defines if all users joining the room will start muted, translates to muteOnStart in BBB
    room_guest_policy = models.CharField(max_length=100,choices=room_guest_policy_choices, default="open") # How can a participant join the room, translates to guestPolicy in BBB
    room_password = models.CharField(max_length=10,blank=True,null=True) # A password for users to join the room
    room_participant_webcam = models.BooleanField(default=True) # Disable webcam for participants , translates to webcamsOnlyForModerator in BBB
    room_all_moderators = models.BooleanField(default=False) # All users are moderators.
    room_session_count = models.PositiveIntegerField(default=0,blank=True,null=True)
    last_session = models.DateTimeField(default=None,null=True,blank=True)
    room_total_storage_size = models.FloatField(default=0.0)

    def __str__(self):
        return "{}({})".format(self.room_name, self.room_user_id)



class RoomHistory(models.Model):
    session = models.ForeignKey(Rooms,on_delete=models.SET_NULL,null=True)
    session_internal_id = models.CharField(max_length=100,default="")
    session_name = models.CharField(max_length=200) # By default set the roomname_datetime, but eventually will be editable.
    session_start_datetime = models.DateTimeField()
    session_end_datetime = models.DateTimeField(blank=True,null=True,default=None)
    session_participants = models.PositiveIntegerField(default=1)
    session_was_recorded = models.BooleanField(default=False)
    session_started_by = models.ForeignKey(User,on_delete=models.SET_NULL,null=True)

    def __str__(self):
        return self.session_name

class RoomRecording(models.Model):
    visibility_options = (
        ('public','Public'),
        ('unlisted','Unlisted'),
        ('private','Private')
        )
    
    name = models.CharField(max_length=1000,blank=True,null=True)
    room = models.ForeignKey(Rooms,on_delete=models.CASCADE)
    recording_id = models.TextField()
    is_active = models.BooleanField(default=False) # This will become true when the recording callback has come.
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    recording_size = models.FloatField(blank=True,null=True) # The size is always stored in bytes
    recording_thumb_1 = models.URLField(blank=True,null=True)
    recording_thumb_2 = models.URLField(blank=True,null=True)
    recording_visibility = models.CharField(max_length=30,choices=visibility_options, default='public')


    def __str__(self):
        return self.recording_id


class Stream(models.Model):
    stream_choices = (
        ('scheduled','Scheduled'),
        ('requested','Requested'),
        ('preparing','Preparing the streamer'),
        ('ready','Streamer Ready, Joining the room'),
        ('joined','Streaming started'),
        ('running','Steam running'),
        ('stopped','Streamer Stopped'),
        ('destroyed','Streamer destroyed')
    )
    stream_room = models.ForeignKey(Rooms,on_delete=models.CASCADE,default=None,null=True)
    stream_id = models.CharField(max_length=100)
    stream_name = models.CharField(max_length=200)
    stream_link = models.CharField(max_length=200)
    stream_start_time = models.DateTimeField(auto_now_add=True)
    stream_end_time = models.DateTimeField(blank=True,null=True)
    stream_status = models.CharField(max_length=50,choices = stream_choices,default="requested")
    stream_server_uuid = models.CharField(max_length=100)
    stream_provider = models.CharField(max_length=20,default="DIGITAL_OCEAN")
    stream_configs = JSONField()

    def __str__(self):
        return self.stream_id

class StreamQuestions(models.Model):
    name = models.CharField(max_length=80)
    comment = models.TextField()
    spam = models.CharField(max_length=1,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    stream = models.ForeignKey(Stream,on_delete=models.CASCADE)


@receiver(post_save, sender=Rooms)
def set_room_specific_details(sender, instance, created, **kwargs):
    if created:
        if not instance.room_user_id:
            #Generate a new room_user_id based automatically
           instance.room_user_id = (str(instance.creator.email.split('@')[0])).lower()[0:3]+"-"+str(instance.room_name.lower()[0:3])+"-"+ str(uuid.uuid4())[0:4]
        instance.room_id = sha256(instance.room_user_id.encode('utf-8')).hexdigest()
        instance.save()

@receiver(post_save, sender=User)
def print_only_after_deal_created(sender, instance, created, **kwargs):
    if created:
        # Create a personal Organisation
        room_user_id = (instance.fullname).replace(" ","_")
        room = Rooms.objects.filter(room_user_id=room_user_id)
        if len(room) > 0:
            no_of_similar_rooms = len(room)
            room_user_id = room_user_id+ (no_of_similar_rooms + 1)
        room = Rooms.objects.create(room_name="Personal Space",room_user_id=room_user_id , room_mute_all_on_start=False, creator=instance, is_active=True)