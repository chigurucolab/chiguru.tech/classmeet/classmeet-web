import time
import json
import xmltodict
import requests
import logging
import hashlib
from django.conf import settings
from django.views.generic import TemplateView
from django.http import request
from django.shortcuts import render
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http.response import JsonResponse
from django.db.models import Count
from cmweb.models import Rooms, RoomRecording, RoomHistory, Stream, StreamQuestions
from cmcore.models import UserOrgMapping, Organisation, User
from cmcore.utils import prepare_for_attendee_join, prepare_url, prepare_for_moderator_join, prepare_for_streamer_join
from .tasks import create_streamer_task, destroy_streamer_task

logger = logging.getLogger('classmeet.cmweb')


def home(request):
    if request.user.is_authenticated:
        return redirect('/app/')
    else:
        return render(request, 'pages/home.html')


class AboutPageView(TemplateView):
    template_name = 'pages/about.html'


class TermsPageView(TemplateView):
    template_name = 'pages/terms.html'


@login_required
def myrooms(request,error=None):
    my_rooms = Rooms.objects.filter(creator=request.user, is_active=True)
    my_recordings = RoomRecording.objects.filter(room__in=my_rooms,is_active=True)
    account_total_storage = 0
    for room in my_rooms:
        account_total_storage = account_total_storage + room.room_total_storage_size
    account_total_storage = str(format(account_total_storage / (1024*1024*1024), ".2f")) + "GB"
    current_active_session_count = my_rooms.filter(is_session_running=True).count()
    return render(request, 'pages/room_rec_base.html', {'my_rooms': my_rooms,'my_recordings':my_recordings,'account_total_storage':account_total_storage, "current_active_session_count":current_active_session_count})


@login_required
@require_http_methods(['POST'])
def create_room(request):
    room_name = request.POST.get("room_name", None)
    room_mute_all_on_start = request.POST.get("room_mute_all_on_start", False)
    if room_mute_all_on_start:
        room_mute_all_on_start = True
    if not room_name:
        # TODO: Need to handle the error here better
        return redirect(reverse("home"))
    room = Rooms.objects.create(
        room_name=room_name, room_mute_all_on_start=room_mute_all_on_start, creator=request.user,room_participant_webcam=False)
    room.is_active = True
    room.save()
    return redirect(reverse("home"))


@login_required
@require_http_methods(['POST'])
def delete_room(request, room_name, room_id):
    room = get_object_or_404(Rooms, room_id=room_id, room_name=room_name)

    if request.method == "POST":
        # confirming delete
        room.is_active = False
        room.save()
        return redirect(reverse("home"))

@login_required
def update_recording(request, recording_id):
    recording = get_object_or_404(RoomRecording, id=recording_id)
    if recording.room.creator == request.user or recording.room.owner == request.user:
        if request.method == "GET":
            return render(request, 'pages/update_recording.html', {'recording': recording})
        elif request.method == "POST":
            recording_name = request.POST.get("recording_name")
            recording_visibility = request.POST.get("recording_visibility","private")
            recording.name = recording_name
            recording.recording_visibility = recording_visibility
            recording.save()
            return redirect(reverse('update_recording',args=[recording_id]))

    else:
        return redirect(reverse("home"))


@login_required
def update_room(request, room_id):
    room = get_object_or_404(Rooms, room_user_id=room_id)
    if room and (room.creator == request.user or room.owner == request.user):
        if request.method == "GET":
                return render(request, 'pages/update_room.html', {'room': room})
        else:
            room_name = request.POST.get("room_name", None)
            room_guest_policy = request.POST.get("room_guest_policy","open")
            room_password = request.POST.get("password",None)
            room_participant_webcam = request.POST.get("room_participant_webcam",False)
            room_all_moderators = request.POST.get("room_all_moderators",False)
            room_mute_all_on_start = request.POST.get("room_mute_all_on_start", False)
            if room_mute_all_on_start:
                room_mute_all_on_start = True
            if room_participant_webcam:
                room_participant_webcam = True
            if room_all_moderators:
                room_all_moderators = True
            if room_guest_policy == "password" and room_password:
                room.room_guest_policy = room_guest_policy
                room.room_password = room_password
            elif room_guest_policy == "password" and not room_password: 
                return render(request, 'pages/update_room.html', {'room': room,'error':'Room password not supplied'})
            else:
                room.room_guest_policy = room_guest_policy
            room.room_mute_all_on_start = room_mute_all_on_start
            room.room_participant_webcam = room_participant_webcam
            room.room_all_moderators = room_all_moderators            
            room.room_name = room_name
            room.save()
            return redirect(reverse("home"))
    else: 
        return redirect(reverse("home"))

def watch(request,stream_id=None):
    if stream_id:
        stream = get_object_or_404(Stream,stream_id=stream_id)
        stream_comments = StreamQuestions.objects.filter(stream=stream)
        return render(request,'pages/watch_live.html',{"stream":stream,'comments':stream_comments})

def stream_ask_question(request):
    name = request.POST.get("name","Anonymous")
    question = request.POST.get("question")
    stream_id = request.POST.get("stream_id")
    spam = request.POST.get("comment",None)
    if question and (spam is None):
        stream = get_object_or_404(Stream,stream_id=stream_id)
        sq = StreamQuestions.objects.create(name=name,comment=question,stream=stream)
        sq.save()
        return JsonResponse({"status":"success"},status=200)
    else:
        return JsonResponse({"status":"failed"},status=500)

@require_http_methods(["GET"])
def get_stream_questions(request,stream_id=None,sqid=None):
    if stream_id:
        stream = get_object_or_404(Stream,stream_id=stream_id)
        if sqid:
            sqs = StreamQuestions.objects.filter(id__gt=sqid,stream=stream).values("name","comment")
        else:
            sqs = StreamQuestions.objects.filter(stream=stream).values("name","comment")
        return JsonResponse({"status":"success","sqs":list(sqs.values_list())},status=200,safe=True)



def join_room(request, room_user_id=None):
    if room_user_id:
        # TODO: Validate if a room ID exists and alert accordingly.
        room = get_object_or_404(Rooms, room_user_id=room_user_id)
        if room:
            if request.method == "GET":
                room_recordings = RoomRecording.objects.filter(room = room,is_active=True, recording_visibility="public")
                return render(request, 'pages/join.html', {'room': room,'room_recordings':room_recordings})
            if request.method == "POST":
                userID = request.POST.get("userId")
                proceed = False
                error = None
                if room.room_guest_policy == "password":
                    password = request.POST.get("password",None)
                    if password and password == room.room_password:
                        proceed = True
                    else: 
                        proceed = False 
                        error = "Password is wrong"
                else:
                    proceed = True
                if proceed: 
                    name = request.POST.get("name", "Anoymous Attendee")
                    muted = "true" if room.room_mute_all_on_start else "false"
                    if room.room_all_moderators:
                        query_params = prepare_for_moderator_join(name, room.room_user_id, userID)
                    else:
                        query_params = prepare_for_attendee_join(name, room.room_user_id, userID, muted=muted)
                    moderator_redirect_url = prepare_url("join", query_params)
                    mod_join_resp = requests.get(moderator_redirect_url)
                    mod_join_resp_json = json.loads(json.dumps(
                        xmltodict.parse(mod_join_resp.content)))
                    if mod_join_resp_json["response"]["returncode"] == "SUCCESS":
                        session_url = mod_join_resp_json["response"]['url']
                        return redirect(session_url)
                else:
                    room_recordings = RoomRecording.objects.filter(room = room,is_active=True, recording_visibility="public")
                    return render(request, 'pages/join.html', {'room': room,'error':error,'room_recordings':room_recordings})

                return "Success. Redirect to actual meeting will come soon"
        else:
            pass  # TODO: Need to handle this
    else:
        pass  # Need to handle things otherwise



def view_recording(request,recording_id):
    recording = RoomRecording.objects.get(id=recording_id)
    if recording:
        if recording.recording_visibility == "public" or recording.recording_visibility == "unlisted":
            return redirect("https://classmeet.chiguru.tech/playback/presentation/2.0/playback.html?meetingId="+recording.recording_id)
        elif recording.recording_visibility == "private":
            if recording.room.owner == request.user or recording.room.creator == request.user:
                return redirect("https://classmeet.chiguru.tech/playback/presentation/2.0/playback.html?meetingId="+recording.recording_id)
            else:
                return render(request,'pages/generic_error.html',{'error':"Either this recording does not exist or you do not have the permission to view the same. Please contact the person who shared the link with you."})
        else: 
            return redirect(reverse('home'))
    else:
        return redirect(reverse('home'))


@login_required
def go_live(request):
    if request.method == "POST":
        request_room = request.POST.get("room")
        room = get_object_or_404(Rooms,id=request_room,creator=request.user)
        youtube_key = request.POST.get("youtube",None)
        facebook_key = request.POST.get("facebook",None)
        if not(youtube_key) and not(facebook_key):
            return redirect(reverse('live'))
        stream_id_raw = room.room_user_id + str(time.time())
        stream_id =  hashlib.md5(stream_id_raw.encode('utf-8')).hexdigest()
        schedule_date = request.POST.get("schedule_date",None)
        if settings.DEBUG:
            stream_id = "dev-"+stream_id
        stream_name = request.POST.get("stream_name","Live Stream of %s"%(room.room_name))
        stream_configs = {}
        if youtube_key:
            stream_configs["youtube"] = youtube_key
        if facebook_key:
            stream_configs["facebook"] = facebook_key
        stream_status = ""
        if schedule_date:
            stream_status = "scheduled"
            stream_start_time = schedule_date
        else:
            stream_status = "requested"
        stream = Stream.objects.create(stream_room = room,stream_id=stream_id, stream_name=stream_name,stream_configs=stream_configs,stream_status=stream_status)
        stream.save()
        if not schedule_date:
            res = create_streamer_task.delay(stream_id,room.room_user_id)
            stream.stream_status = "preparing"
            stream.save()
        return redirect(reverse('live'))

    else:
        # TODO: Dont show the rooms where stream is happening. Add a is_streaming_flag to the room details ?
        running_rooms = Rooms.objects.filter(creator=request.user,is_session_running=True)
        all_rooms = Rooms.objects.filter(creator=request.user)
        current_streams = Stream.objects.filter(stream_room__creator=request.user).exclude(stream_status__in=["stopped","destroyed","scheduled"])
        scheduled_streams = Stream.objects.filter(stream_room__creator=request.user,stream_status="scheduled")
        return render(request, 'pages/go_live.html',{"rooms":running_rooms,"streams":current_streams,"all_rooms":all_rooms,"scheduled_streams":scheduled_streams})

@login_required
def start_scheduled_stream(request):
    stream_id = request.POST.get("stream_id",None)
    stream = get_object_or_404(Stream,stream_id=stream_id,stream_room__creator = request.user)
    res = create_streamer_task.delay(stream_id,stream.stream_room.room_user_id)
    stream.stream_status = "preparing"
    stream.save()
    return redirect(reverse('live'))

@login_required
def destroy_streamer(request):
    stream_id = request.POST.get("stream_id")
    stream = get_object_or_404(Stream,stream_id=stream_id)
    delete = False
    if stream.stream_status:
        delete = True
    stream.stream_status = "stopped"
    # We donot have to store the stream key ever and should NEVER store it.
    stream.stream_configs = {"":""}
    stream.save()
    if delete:
        res = destroy_streamer_task.delay(stream_id)
    return redirect(reverse('go_live'))

@login_required
@require_http_methods(["GET"])
def stream_comments_moderator(request,stream_id):
    if stream_id:
        stream = get_object_or_404(Stream,stream_id=stream_id, stream_room__creator=request.user)
        stream_comments = StreamQuestions.objects.filter(stream=stream)
        return render(request,'pages/comments.html',{"stream":stream,'comments':stream_comments})

@csrf_exempt
def get_streamer_link(request):
    # TODO: Validation needs to be added
    stream_id = request.POST.get("stream_id",None)
    api_key = request.POST.get("api_key","hello")
    if api_key and api_key == settings.STREAM_API_KEY:
        if stream_id:
            stream = get_object_or_404(Stream, stream_id=stream_id)
            if stream:
                response = {}
                if stream.stream_room.is_session_running:
                    room_params = prepare_for_streamer_join(stream.stream_room.room_user_id)
                    room_link = prepare_url("join",room_params)
                    response["message"] = room_link
                    if "youtube" in stream.stream_configs:
                        response["youtube"] = stream.stream_configs["youtube"]
                    if "facebook" in stream.stream_configs:
                        response["facebook"] = stream.stream_configs["facebook"]
                    response["status"] = "success"
                    return JsonResponse(response,status=200)
                else:
                    return JsonResponse({"status":"failed","message":"Meeting not started.Retry later"},status=403)
            else:
                return JsonResponse({"status":"failed","message":"Meeting id not found"},status=404)
        else:
            return JsonResponse({"status":"failed","message":"No Stream ID Provided"},status=500)
    else:
        return JsonResponse({"status":"failed","message":"API Key mismatch. Failed processing the request"},status=403)



# OLD CODE: Retained for historical purposes
# @csrf_exempt
# def streamer_ready(request):
#     stream_id = request.POST.get("stream_id")
#     pub_ip = request.POST.get("ip")
#     stream_server_uuid = request.POST.get("stream_server_uuid")
#     stream = get_object_or_404(Stream,stream_id=stream_id)
#     if stream:
#         stream.stream_status = "ready"
#         stream.stream_link = pub_ip
#         stream.stream_server_uuid = stream_server_uuid
#         stream.save()
#         return JsonResponse({"status":"success","message":"Steam ID: %s updated"%stream_id},status=200)
#     else:
#         res = destroy_streamer.delay(stream_id)
#         return JsonResponse({"status":"failed","message":"Stream ID: %s not found"%stream_id},status=404)



# @csrf_exempt
# def finished_destroying(request):
#     stream_id = request.POST.get("stream_id")
#     stream = get_object_or_404(Stream,stream_id=stream_id)
#     stream.stream_status = "destroyed"
#     stream.save()
#     return JsonResponse({"status":"success","message":"Steam ID: %s server destroyed"%stream_id},status=200)
